# Craftable Stuff

The Craftable Stuff Mod
<br>
For Minecraft 1.12.2

## Recipes


![](https://gitlab.com/sukeban/mc-lab/craftablestuff/-/raw/master/recipe_lead.png) 
<br>
![](https://gitlab.com/sukeban/mc-lab/craftablestuff/-/raw/master/recipe_strings.png)