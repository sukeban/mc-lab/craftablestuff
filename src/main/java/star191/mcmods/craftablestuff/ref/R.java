package star191.mcmods.craftablestuff.ref;

public class R {
    public static final String MOD_ID = "craftablestuff";
    public static final String MAIN_PACKAGE_PREFIX = "star191.mcmods.craftablestuff";
    public static final String CLIENT_PROXY_CLASS = MAIN_PACKAGE_PREFIX + "." + "proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = MAIN_PACKAGE_PREFIX + "." + "proxy.ServerProxy";

    public static final boolean CAN_BE_DEACTIVATED = true;
}
