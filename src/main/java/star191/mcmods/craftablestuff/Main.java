package star191.mcmods.craftablestuff;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;
import star191.mcmods.craftablestuff.proxy.IProxy;
import star191.mcmods.craftablestuff.ref.R;
import star191.mcmods.craftablestuff.utils.LogHelper;

@Mod(
        modid = R.MOD_ID,
        canBeDeactivated = R.CAN_BE_DEACTIVATED
)
public class Main
{

    @Mod.Instance
    public static Main instance = new Main();

    @SidedProxy(clientSide = R.CLIENT_PROXY_CLASS, serverSide = R.SERVER_PROXY_CLASS)
    public static IProxy proxy;

    private static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        LogHelper.info("preInit");
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        LogHelper.info("init");
        proxy.init(event);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event){
        LogHelper.info("postInit");
    }
}
