package star191.mcmods.craftablestuff.proxy;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;

public interface IProxy {
    void init(FMLInitializationEvent event);
}
