package star191.mcmods.craftablestuff.proxy;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import star191.mcmods.craftablestuff.utils.CraftingRegister;

public abstract class CommonProxy implements IProxy{
    @Override
    public void init(FMLInitializationEvent event) {
        CraftingRegister.register();
    }
}
