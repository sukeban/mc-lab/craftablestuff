package star191.mcmods.craftablestuff.utils;
import net.minecraftforge.fml.common.FMLLog;
import org.apache.logging.log4j.Level;
import star191.mcmods.craftablestuff.ref.R;

public class LogHelper {
    public static final boolean DEBUG = false;

    public static void log(Level logLevel, Object o) {
        FMLLog.log(R.MOD_ID, logLevel, String.valueOf(o));
    }

    public static void all(Object message) {
        log(Level.ALL, message);
    }

    public static void debug(Object message) {
        if (DEBUG)
            log(Level.ERROR, message);
    }

    public static void fatal(Object message) {
        log(Level.FATAL, message);
    }

    public static void info(Object message) {
        log(Level.INFO, message);
    }

    public static void off(Object message) {
        log(Level.OFF, message);
    }

    public static void trace(Object message) {
        log(Level.TRACE, message);
    }

    public static void warn(Object message) {
        log(Level.WARN, message);
    }
}
