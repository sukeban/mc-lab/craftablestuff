package star191.mcmods.craftablestuff.utils;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IRecipeFactory;
import net.minecraftforge.fml.common.registry.GameRegistry;
import star191.mcmods.craftablestuff.ref.R;

public class CraftingRegister {
    public static void register() {
        LogHelper.info("register()");

        //registerRecipes("craftablelead");
        //registerRecipes("craftablestring");

        GameRegistry.addShapedRecipe(
                new ResourceLocation(R.MOD_ID, "craftablelead"),
                null,
                new ItemStack(Items.LEAD, 1),
                new Object[] {"SS ", "SW ", "  S", 'W', Item.getItemFromBlock(Blocks.WOOL), 'S', Items.STRING});

        GameRegistry.addShapelessRecipe(
                new ResourceLocation(R.MOD_ID, "craftablestring"),
                null,
                new ItemStack(Items.STRING, 3),
                Ingredient.fromItem(Item.getItemFromBlock(Blocks.WOOL)));
    }

    public static void registerRecipes(final String name) {
        LogHelper.info("registerRecipes()");
        CraftingHelper.register(
                new ResourceLocation(R.MOD_ID, name),
                (IRecipeFactory) (context, json) -> CraftingHelper.getRecipe(json, context)
        );
    }
}
